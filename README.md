# API Empresas (v1)

API para listagem de empresas, através de investidores autenticados.

### Principais Tecnologias
- [Express](https://expressjs.com/pt-br/)
- [JWT](https://jwt.io/) ([jsonwebtoken](https://github.com/auth0/node-jsonwebtoken))
- [Typeorm](https://typeorm.io/)

### Intalação
Faça o clone do projeto, acesse a pasta do mesmo e execute o comando a seguir para instalar as dependências:  
`$ yarn` ou `$ npm install`  

Crie um banco de dados e configure o arquivo `ormconfig.json`. Após configuração, execute o comando a seguir para geração das tabelas:  
`$ yarn typeorm migration:run`  

Para executar o servidor em modo de desenvolvimento, execute o comando:  
`yarn dev:server`  

### Considerações

- Escolhi uma estrutra que pode parecer overengeniering, devido ao pequeno tamanho da aplicação mas foi para mostrar uma separação mais amigável para escalabilidade. Pasta "modules" com os domínios da aplicação e dentro, pastas para separar responsabilidades. Pasta "shared" contendo partes comuns à aplicação.
- Adicionei várias configurações de prettier, eslinte e editorconfig para padronização de código: pode ser modificado fácilmente a gosto da equipe.
- Esbocei uma DER (com a ferramenta [dbdiagram.io](https://dbdiagram.io/)) para ter noção de como criar as entities do Typeorm e montar as migrations. Não ficou exatamente igual deivo à forma de utilização e convenção do TypeORM. Veja a imagem na raiz do projeto com o nome "DER-Ioasys-Enterprises.png".
- Por questões de convenção do JWT, utilizei o cabeçalho Authorization e o valor com a hash prefixada por espaço em branco e a palavara "Bearer". Ex.: Authorization: Bearer\[espaço_em_branco\]<access-token>. Por este motivo alterei o arquivo de collection do postman (está na raiz, no formato JSON), adicionando a variável access-token para a aba Authorization que possui config própria para token Bearer.
- Removi também o trecho inicial na aba Tests que necessitava da access-token em cada retorno de requisição e estava limpando as variáveis. Como o token JWT já mantem parte da hash para salvarmos dados do usuário, não estou usando os headers uid e client (se for má prática, um feedback seria bem vindo. 😉
- Não sei como estão criptografando as senhas na API, usei a lib bcrypt. Para testar, salvei no banco utilizando uma hash gerada [neste site](https://www.browserling.com/tools/bcrypt).
- Adicionei paginação na rota "Enterprise index", apesar de não estar no escopo, com parametros skip e take, já que a requisição pode listar muitas empresas e pesar a requisição.
- No mais, criei separações de regras nos services e utilizei o Typeorm (poderia ser Sequelize, Knex, etc.) para comunicação com o banco de dados PostgreSQL, que facilita e simplifica a interação com os bancos de dados, além de facilitar em uma possível (muitas vezes improvável) troca de SGBD.
- Não coloquei muitos comentários no código pois a aplicação é pequena e acredito que está simples de entender.

### Considerações finais
- Gostei do teste e da disponibilização de um API para teste e do arquivo de collection do Postman. Ficou bem legal!!!
- Podem estar se perguntando onde estão os testes. Infelizmente, como trabalho de 8h às 18h (ou mais), só tive tempo em parte das noites para fazer o teste. Com mais tempo eu teria feito melhorias, os teste unitários (métodos e funcções) e de integração (nas rotas) e criado os seeds também (talvez com uma lib como esta: https://github.com/w3tecch/typeorm-seeding#-basic-seeder)
- Não conhecia esse recurso do Postman para executar scripts (configurar variáveis de ambiente) pós requisições, bem legal! Se eu não passar no teste do processo seletivo, já valeu o teste. =)
- Qualquer sugestão, ficarei feliz em aprender. Obrigado!!! 👍

Dayvis Apolinário  
[contato@dayvisapolinario.dev](mailto:contato@dayvisapolinario.dev)


_____________________________________________________________________
# README

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto Empresas.

### O QUE FAZER ?

- Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso, nós iremos avaliar e retornar por email o resultado do seu teste.

### ESCOPO DO PROJETO

- Deve ser criada uma API em **NodeJS** ou **Ruby on Rails**.
- A API deve fazer o seguinte:

1. Login e acesso de Usuário já registrado;
2. Para o login usamos padrões **JWT** ou **OAuth 2.0**;
3. Listagem de Empresas
4. Detalhamento de Empresas
5. Filtro de Empresas por nome e tipo

### Informações Importantes

- A API deve funcionar exatamente da mesma forma que a disponibilizada na collection do postman, mais abaixo os acessos a API estarão disponíveis em nosso servidor.

  - Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers (access-token, client, uid);

  - Para ter acesso as demais APIS precisamos enviar esses 3 custom headers para a API autorizar a requisição;

- Mantenha a mesma estrutura do postman em sua API, ou seja, ela deve ter os mesmo atributos, respostas, rotas e tratamentos, funcionando igual ao nosso exemplo.

- Quando seu código for finalizado e disponibilizado para validarmos, vamos subir em nosso servidor e realizar a integração com o app.

- Independente de onde conseguiu chegar no teste é importante disponibilizar seu fonte para analisarmos.

- É obrigatório utilização de Banco de Dados MySql/PostgreSQL

### Dados para Teste

- Servidor: https://empresas.ioasys.com.br/
- Versão da API: v1
- Usuário de Teste: testeapple@ioasys.com.br
- Senha de Teste : 12341234

### Dicas

- Documentação JWT https://jwt.io/
- Frameworks NodeJS:

  1. https://expressjs.com/pt-br/
  2. https://sailsjs.com/

- Guideline rails http://guides.rubyonrails.org/index.html
- Componente de autenticação https://github.com/lynndylanhurley/devise_token_auth
