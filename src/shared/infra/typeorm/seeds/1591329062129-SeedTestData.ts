import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import User from '../../../../modules/users/infra/typeorm/entities/User';
import Investor from '../../../../modules/investor/infra/typeorm/entities/Investor';

export default class CreateUsers implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const inserts = await connection
      .createQueryBuilder()
      .insert()
      .into(User)
      .values([
        {
          email: 'testeapple@ioasys.com.br',
          password: '$2a$08$yRTIV4QsPr7PhxelsNvj7O7BGXHmV9WZja3nmdwj.xcY5r1E.FN3i',
          avatar: undefined,
          first_access: undefined,
          created_at: '2020-06-04 20:30:44.422',
          updated_at: '2020-06-04 20:30:44.422',
        }
      ])
      .returning("INSERTED.*")
      .printSql()
      .execute();


      console.log('inserts ===>>>>>>', inserts);


      await connection
      .createQueryBuilder()
      .insert()
      .into(Investor)
      .values([
        {
          investor_name: 'Teste Apple',
          city: 'BH',
          country: 'Brasil',
          balance: 350000,
          photo: '/uploads/investor/photo/1/cropped4991818370070749122.jpg',
          super_angel: false,
          created_at: '2020-06-04 20:50:11.986',
          updated_at: '2020-06-04 20:50:11.986',
          user: ,
        }
      ])
      .returning("INSERTED.*")
      .printSql()
      .execute();
  }
}


//TODO: Criar seeds para as queries abaixo:
// INSERT INTO public.users (email,"password",avatar,first_access,created_at,updated_at) VALUES
// ('testeapple@ioasys.com.br','$2a$08$yRTIV4QsPr7PhxelsNvj7O7BGXHmV9WZja3nmdwj.xcY5r1E.FN3i',NULL,NULL,'2020-06-04 20:30:44.422','2020-06-04 20:30:44.422')
// RETURNING id
// ;

// INSERT INTO public.investors (investor_name,city,country,balance,photo,super_angel,created_at,updated_at,"userId") VALUES
// ('Teste Apple','BH','Brasil',350000,'/uploads/investor/photo/1/cropped4991818370070749122.jpg',false,'2020-06-04 20:50:11.986','2020-06-04 20:50:11.986',
// 1
// )
// ;

// INSERT INTO public.enterprise_type (enterprise_type_name,created_at,updated_at) VALUES
// --('Software','2020-06-04 23:36:32.282','2020-06-04 23:36:32.282'),
// --('Health','2020-06-04 23:36:47.846','2020-06-04 23:36:47.846'),
// ('Fashion','2020-06-04 23:36:47.846','2020-06-04 23:36:47.846')
// ;

// INSERT INTO public.enterprises (email_enterprise,facebook,twitter,linkedin,phone,own_enterprise,enterprise_name,photo,description,city,country,value,share_price,created_at,updated_at,"enterpriseTypeId") VALUES
// --('',' ',' ',' ',' ',false,'AllRide','/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg','Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry','Santiago','Chile',0,5000,'2020-06-04 23:43:18.160','2020-06-04 23:43:18.160',1),
// ('',' ',' ',' ',' ',false,'Alpaca Samka SpA','/uploads/enterprise/photo/2/WhatsApp_Image_2017-10-31_at_13.47.22.jpeg','Alpaca Samka uses alpaca fibres for our “Slow Fashion Project” in association with the Aymaras of the Chilean Andes, producing sustainable luxury accessories and garments using traditional Andean methods and British weaving patterns. We are part of the Inward Investment Program and have been recognised by international organisations.','Viña del Mar','Chile',0,5000,'2020-06-04 23:43:18.160','2020-06-04 23:43:18.160',3)
// ;

// INSERT INTO public.investors_portfolio_enterprises ("investorsId","enterprisesId") VALUES
// (1,1)
// ;
