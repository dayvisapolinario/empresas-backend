import { MigrationInterface, QueryRunner } from 'typeorm';

export default class CreateTables1591317216680 implements MigrationInterface {
  name = 'CreateTables1591317216680';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE DATABASE "enterprise"`);
    await queryRunner.query(
      `CREATE TABLE "enterprise_type" ("id" SERIAL NOT NULL, "enterprise_type_name" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_fa846ffd5ab8791dcb5f309a69b" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "enterprises" ("id" SERIAL NOT NULL, "email_enterprise" character varying NOT NULL, "facebook" character varying NOT NULL, "twitter" character varying NOT NULL, "linkedin" character varying NOT NULL, "phone" character varying NOT NULL, "own_enterprise" boolean NOT NULL, "enterprise_name" character varying NOT NULL, "photo" character varying NOT NULL, "description" character varying NOT NULL, "city" character varying NOT NULL, "country" character varying NOT NULL, "value" integer NOT NULL, "share_price" integer NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "enterpriseTypeId" integer, CONSTRAINT "REL_2f65a076d77662155784c7a5da" UNIQUE ("enterpriseTypeId"), CONSTRAINT "PK_a019e9afe6517b4f2a4588f2cce" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "users" ("id" SERIAL NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "avatar" character varying, "first_access" TIMESTAMP, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "investors" ("id" SERIAL NOT NULL, "investor_name" character varying NOT NULL, "city" character varying NOT NULL, "country" character varying NOT NULL, "balance" integer NOT NULL, "photo" character varying NOT NULL, "super_angel" boolean NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "userId" integer, CONSTRAINT "REL_5fc2494c4bcbbca3c4729921ff" UNIQUE ("userId"), CONSTRAINT "PK_7ab129212e4ce89e68d6a27ea4e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "investors_portfolio_enterprises" ("investorsId" integer NOT NULL, "enterprisesId" integer NOT NULL, CONSTRAINT "PK_30779487fe05615fb7a02532535" PRIMARY KEY ("investorsId", "enterprisesId"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_91631895a29b6df67a4dc7710d" ON "investors_portfolio_enterprises" ("investorsId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_52f3640dac203cf065018822e0" ON "investors_portfolio_enterprises" ("enterprisesId") `,
    );
    await queryRunner.query(
      `ALTER TABLE "enterprises" ADD CONSTRAINT "FK_2f65a076d77662155784c7a5da7" FOREIGN KEY ("enterpriseTypeId") REFERENCES "enterprise_type"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "investors" ADD CONSTRAINT "FK_5fc2494c4bcbbca3c4729921ff2" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "investors_portfolio_enterprises" ADD CONSTRAINT "FK_91631895a29b6df67a4dc7710db" FOREIGN KEY ("investorsId") REFERENCES "investors"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "investors_portfolio_enterprises" ADD CONSTRAINT "FK_52f3640dac203cf065018822e0e" FOREIGN KEY ("enterprisesId") REFERENCES "enterprises"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "investors_portfolio_enterprises" DROP CONSTRAINT "FK_52f3640dac203cf065018822e0e"`,
    );
    await queryRunner.query(
      `ALTER TABLE "investors_portfolio_enterprises" DROP CONSTRAINT "FK_91631895a29b6df67a4dc7710db"`,
    );
    await queryRunner.query(
      `ALTER TABLE "investors" DROP CONSTRAINT "FK_5fc2494c4bcbbca3c4729921ff2"`,
    );
    await queryRunner.query(
      `ALTER TABLE "enterprises" DROP CONSTRAINT "FK_2f65a076d77662155784c7a5da7"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_52f3640dac203cf065018822e0"`);
    await queryRunner.query(`DROP INDEX "IDX_91631895a29b6df67a4dc7710d"`);
    await queryRunner.query(`DROP TABLE "investors_portfolio_enterprises"`);
    await queryRunner.query(`DROP TABLE "investors"`);
    await queryRunner.query(`DROP TABLE "users"`);
    await queryRunner.query(`DROP TABLE "enterprises"`);
    await queryRunner.query(`DROP TABLE "enterprise_type"`);
    await queryRunner.query(`DROP DATABASE "enterprise"`);
  }
}
