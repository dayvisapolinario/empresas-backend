import { Router } from 'express';
import sessionsRouterV1 from './v1/sessions.routes';
import enterprisesRouterV1 from './v1/enterprises.routes';
// import usersRouterV1 from './v1/users.routes';

const routes = Router();

routes.use('/users/auth/sign_in', sessionsRouterV1);
// routes.use('/users', usersRouterV1);
routes.use('/enterprises', enterprisesRouterV1);

routes.use('/api/v1', routes);

export default routes;
