/* eslint-disable camelcase */
import { Router } from 'express';

import ensureAuthenticated from '../../middlewares/ensureAuthenticated';
import GetEnterpriseService from '../../../../../modules/enterprises/services/GetEnterpriseService';
import ListEnterprisesService from '../../../../../modules/enterprises/services/ListEnterprisesService';

const enterprisesRouter = Router();

enterprisesRouter.get(
  '/:id?',
  ensureAuthenticated,
  async (request, response) => {
    const { id } = request.params;
    const { enterprise_types, name, skip, take } = request.query;

    if (id) {
      const getEnterpriseService = new GetEnterpriseService();
      const enterprise = await getEnterpriseService.execute(+id);
      response.json(enterprise);
    }

    const listEnterprisesService = new ListEnterprisesService();
    const enterprises = await listEnterprisesService.execute({
      enterprise_types,
      name,
      skip,
      take,
    });

    return response.json(enterprises);
  },
);

export default enterprisesRouter;
