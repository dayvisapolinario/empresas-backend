import { Router } from 'express';
import AuthenticateUserService from '../../../../../modules/users/services/AuthenticateUserService';

const sessionsRouter = Router();

sessionsRouter.post('/', async (request, response) => {
  const { email, password } = request.body;

  const authenticateUser = new AuthenticateUserService();

  const { investor, token } = await authenticateUser.execute({
    email,
    password,
  });

  const investorResponse = { ...investor, email };

  response.header('token-type', 'Bearer');
  response.header('access-token', token);
  response.header('client', investor.id.toString());
  response.header('uid', email);

  return response.json({ investorResponse, enterprise: null, success: true });
});

export default sessionsRouter;
