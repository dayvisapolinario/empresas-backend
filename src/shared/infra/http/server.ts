import 'reflect-metadata';
import express, { Request, Response, NextFunction } from 'express';
import 'express-async-errors';

import routes from './routes';
import AppErrors from '../../errors/AppErrors';

import '../typeorm';

const app = express();

const port = 3333;

app.use(express.json());
app.use(routes);

app.use((err: Error, request: Request, response: Response, _: NextFunction) => {
  if (err instanceof AppErrors) {
    return response.status(err.statusCode).json({
      status: err.statusCode,
      error: err.message,
    });
  }

  console.error(err);

  return response.status(500).json({
    status: 'error',
    message: 'Internal server error',
  });
});

app.listen(port, () => {
  console.log(`Server started on port ${port}!`);
});
