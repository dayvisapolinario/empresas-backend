import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  ManyToMany,
  JoinColumn,
  JoinTable,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

import User from '../../../../users/infra/typeorm/entities/User';
import Enterprise from '../../../../enterprises/infra/typeorm/entities/Enterprise';

@Entity('investors')
class Investor {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(type => User)
  @JoinColumn()
  user: User;

  /**
   * Portfolio: It is the group of companies
   *  that the investor has investments in.
   */
  @ManyToMany(type => Enterprise)
  @JoinTable()
  portfolio: Enterprise[];

  @Column()
  investor_name: string;

  @Column()
  city: string;

  @Column()
  country: string;

  @Column()
  balance: number;

  @Column()
  photo: string;

  @Column()
  super_angel: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Investor;
