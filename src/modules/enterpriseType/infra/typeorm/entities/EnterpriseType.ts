import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('enterprise_type')
class EnterpriseType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  enterprise_type_name: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default EnterpriseType;
