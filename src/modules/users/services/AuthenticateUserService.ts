import { getRepository } from 'typeorm';
import { compare } from 'bcryptjs';
import { sign } from 'jsonwebtoken';

import authConfig from '../../../config/auth';
import AppErrors from '../../../shared/errors/AppErrors';
import User from '../infra/typeorm/entities/User';
import Investor from '../../investor/infra/typeorm/entities/Investor';

interface Request {
  email: string;
  password: string;
}

interface Response {
  investor: Investor;
  enterprise: unknown;
  token: string;
}

class AuthenticateUserService {
  public async execute({ email, password }: Request): Promise<Response> {
    const usersRepository = getRepository(User);

    const user = await usersRepository.findOne({
      where: { email },
    });

    if (!user) {
      throw new AppErrors('Incorrect email/password combination', 401);
    }

    const passwordMatched = await compare(password, user.password);

    if (!passwordMatched) {
      throw new AppErrors('Incorrect email/password combination', 401);
    }

    const investorRepository = getRepository(Investor);

    const investor = await investorRepository.findOne({
      where: { user: user.id },
    });

    if (!investor) {
      throw new AppErrors('Investor was not found', 404);
    }

    const { secret, expiresIn } = authConfig.jwt;

    const token = sign({}, secret, {
      subject: user.email,
      expiresIn,
    });

    delete investor.created_at;
    delete investor.updated_at;

    return {
      investor,
      enterprise: null,
      token,
    };
  }
}

export default AuthenticateUserService;
