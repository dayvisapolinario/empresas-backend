import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

import EnterpriseType from '../../../../enterpriseType/infra/typeorm/entities/EnterpriseType';

@Entity('enterprises')
class Enterprise {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(type => EnterpriseType, {
    eager: true,
  })
  @JoinColumn()
  enterpriseType: EnterpriseType;

  @Column()
  email_enterprise: string;

  @Column()
  facebook: string;

  @Column()
  twitter: string;

  @Column()
  linkedin: string;

  @Column()
  phone: string;

  @Column()
  own_enterprise: boolean;

  @Column()
  enterprise_name: string;

  @Column()
  photo: string;

  @Column()
  description: string;

  @Column()
  city: string;

  @Column()
  country: string;

  @Column()
  value: number;

  @Column()
  share_price: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Enterprise;
