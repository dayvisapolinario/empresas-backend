/* eslint-disable camelcase */
import { getRepository } from 'typeorm';

import Enterprise from '../infra/typeorm/entities/Enterprise';

interface Request {
  enterprise_types?: string;
  name?: string;
  skip?: number | undefined;
  take?: number | undefined;
}

interface Filter {
  enterpriseType?: { id: string };
  enterprise_name?: string;
}

class ListEnterprisesService {
  public async execute({
    enterprise_types,
    name,
    skip,
    take,
  }: Request): Promise<Enterprise[]> {
    const enterprisesRepository = getRepository(Enterprise);

    const filter: Filter = {};

    if (enterprise_types) filter.enterpriseType = { id: enterprise_types };
    if (name) filter.enterprise_name = name;
    // if (start) .enterprise_name = start;
    // if (limit) pagging.enterprise_name = limit;

    const enterprises = await enterprisesRepository.find({
      where: filter,
      skip,
      take,
    });

    return enterprises;
  }
}

export default ListEnterprisesService;
