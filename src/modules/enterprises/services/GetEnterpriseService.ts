import { getRepository } from 'typeorm';

import AppErrors from '../../../shared/errors/AppErrors';
import Enterprise from '../infra/typeorm/entities/Enterprise';

class GetEnterpriseService {
  public async execute(id: number): Promise<Enterprise> {
    const enterprisesRepository = getRepository(Enterprise);

    const enterprise = await enterprisesRepository.findOne(id);

    if (!enterprise) {
      throw new AppErrors('Not found', 404);
    }

    return enterprise;
  }
}

export default GetEnterpriseService;
